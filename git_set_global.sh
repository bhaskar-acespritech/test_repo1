#!/bin/bash
USER_EMAIL="EMAIL"
USER_NAME="USERNAME"

echo "Changing the Git Global user.email to : $USER_EMAIL"
git config --global user.email $USER_EMAIL
echo "Changing the Git Global user.name to : $USER_NAME"
git config --global user.name $USER_NAME
